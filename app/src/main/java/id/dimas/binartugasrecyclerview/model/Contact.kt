package id.dimas.binartugasrecyclerview.model

data class Contact(
    val nama: String,
    val phone: String,
    val photo: Int
)
