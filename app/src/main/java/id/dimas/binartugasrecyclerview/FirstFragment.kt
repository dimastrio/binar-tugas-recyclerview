package id.dimas.binartugasrecyclerview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import id.dimas.binartugasrecyclerview.adapter.ContactAdapter
import id.dimas.binartugasrecyclerview.databinding.FragmentFirstBinding
import id.dimas.binartugasrecyclerview.model.Contact


class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    private val listContacs = arrayListOf<Contact>(
        Contact("Adam Sautin", "081546731894", R.drawable.ic_person),
        Contact("Abdul Slam Rifai", "081574253263", R.drawable.ic_person),
        Contact("Dani Nugroho", "081346552368", R.drawable.ic_person),
        Contact("Edy Wardhana", "081513653377", R.drawable.ic_person),
        Contact("Harry Lasmana", "081509722753", R.drawable.ic_person),
        Contact("Ivan Setiawan", "081126811578", R.drawable.ic_person),
        Contact("Jeri Hartono", "081846026962", R.drawable.ic_person),
        Contact("John Surjana", "081752946922", R.drawable.ic_person)
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }

    private fun initRecyclerView() {
        val contactAdapter = ContactAdapter(listContacs)
        binding.rvData.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = contactAdapter
        }
    }


}