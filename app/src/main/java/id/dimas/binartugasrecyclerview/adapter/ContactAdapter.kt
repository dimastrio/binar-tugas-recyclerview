package id.dimas.binartugasrecyclerview.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.dimas.binartugasrecyclerview.R
import id.dimas.binartugasrecyclerview.model.Contact

class ContactAdapter(val listContact: ArrayList<Contact>) :
    RecyclerView.Adapter<ContactAdapter.ContactViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
        return ContactViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.bind(listContact[position])
    }

    override fun getItemCount(): Int = listContact.size


    inner class ContactViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvPhoto: ImageView = view.findViewById(R.id.tv_photo)
        val tvNama: TextView = view.findViewById(R.id.tv_name)
        val tvPhone: TextView = view.findViewById(R.id.tv_phone)

        fun bind(item: Contact) {
            tvPhoto.setImageResource(item.photo)
            tvNama.text = item.nama
            tvPhone.text = item.phone
        }
    }
}